from django.test import TestCase
from django.urls import reverse
from django.contrib.auth.models import User
from .models import Book, Rental, AddedUser

class ViewsTestCase(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(username='testuser', password='password123')
        self.admin_user = AddedUser.objects.create(name='admin', email='admin@example.com', role='Admin')
        self.student_user = AddedUser.objects.create(name='student', email='student@example.com', role='Student')

        self.book = Book.objects.create(title='Test Book', author='Test Author', number_of_pages=100)
        self.rental = Rental.objects.create(user=self.student_user, book=self.book)

    def test_student_dashboard(self):
        # Test student dashboard access
        self.client.login(username='student@example.com', password='password123')
        response = self.client.get(reverse('student_dashboard'))
        self.assertEqual(response.status_code, 200)

        # Test book renting
        response = self.client.post(reverse('student_dashboard'), {'book_title': 'Test Book'})
        self.assertEqual(response.status_code, 200)

    def test_admin_dashboard(self):
        # Test admin dashboard access
        self.client.login(username='admin@example.com', password='password123')
        response = self.client.get(reverse('admin_dashboard'))
        self.assertEqual(response.status_code, 302)

        # Test adding a new book
        response = self.client.post(reverse('admin_dashboard'), {'title': 'New Book', 'author': 'New Author', 'number_of_pages': 200, 'add_book': True})
        self.assertEqual(response.status_code, 302)  # Should redirect to admin dashboard

        # Test changing rental date
        response = self.client.post(reverse('admin_dashboard'), {'rental_id': self.rental.id, 'new_rental_date': '2024-04-30', 'change_rental_date': True})
        self.assertEqual(response.status_code, 302)  # Should redirect to admin dashboard

    def test_login(self):
        # Test login with valid credentials
        response = self.client.post(reverse('user_login'), {'username': 'student@example.com', 'password': 'password123'})
        self.assertEqual(response.status_code, 200)  # Should redirect to student dashboard

        # Test login with invalid credentials
        response = self.client.post(reverse('user_login'), {'username': 'invaliduser', 'password': 'invalidpassword'})
        self.assertEqual(response.status_code, 200)  # Should stay on login page

    def test_rented_books_details(self):
        # Test viewing rented books for a student
        self.client.login(username='student@example.com', password='password123')
        response = self.client.get(reverse('rented_books_details'))
        self.assertEqual(response.status_code, 302)

    def test_logout(self):
        # Test logout
        self.client.login(username='student@example.com', password='password123')
        response = self.client.post(reverse('student_dashboard'), {'logout': True})
        self.assertEqual(response.status_code, 302)  # Should redirect to login page
