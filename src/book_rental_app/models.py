from django.db import models
from django.utils import timezone

ROLE_CHOICES = (
    ('Student', 'Student'),
    ('Admin','Admin')
)
class AddedUser(models.Model):
    name = models.CharField(max_length=100)
    email = models.EmailField(unique=True)
    role = models.CharField(max_length=20, choices=ROLE_CHOICES, default="Student")

    def __str__(self):
        return self.email

class Book(models.Model):
    title = models.CharField(max_length=200)
    author = models.CharField(max_length=100)
    number_of_pages = models.IntegerField()

    def __str__(self):
        return self.title

class Rental(models.Model):
    user = models.ForeignKey(AddedUser, on_delete=models.CASCADE)
    book = models.ForeignKey(Book, on_delete=models.CASCADE)
    rental_date = models.DateTimeField(default=timezone.now)
    return_date = models.DateTimeField(null=True, blank=True)
    is_returned = models.BooleanField(default=False)

    def calculate_rental_fee(self): 
        if self.return_date is None:
            return 0
        else:
            days_rented = (self.return_date - self.rental_date).days
            if days_rented <= 30:
                return 0
            else:
                fee = (self.book.number_of_pages / 100) * ((days_rented - 30) / 30)
                return fee

    def __str__(self):
        return f"{self.user.name} rented {self.book.title}"
