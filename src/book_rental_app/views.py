from django.shortcuts import render

# Create your views here.
# views.py

from django.contrib.auth.decorators import login_required

from django.contrib.auth import authenticate, login
from django.shortcuts import render, redirect
from django.urls import reverse
from .models import AddedUser
import requests
from .models import Book, Rental, AddedUser
from urllib.parse import quote
from django.utils import timezone
from datetime import timedelta
from django.contrib.auth import logout

from django.contrib.auth.models import AnonymousUser

def home(request):
    if request.user.is_authenticated:
        try:
            get_added_user = AddedUser.objects.get(email=request.user.email)
            return render(request, 'home.html', {'get_added_user': get_added_user})
        except AddedUser.DoesNotExist:
            return render(request, 'home.html', {'error_message': 'User not found'})
    else:
        return render(request, 'home.html')


def user_login(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            try:
                added_user = AddedUser.objects.get(name=username)
                if added_user.role == 'Admin':
                    return redirect('admin_dashboard')
                elif added_user.role == 'Student':
                    return redirect('student_dashboard')
            except AddedUser.DoesNotExist:
                return render(request, 'login.html', {'error': 'User role not found'})
        else:
            return render(request, 'login.html', {'error': 'Invalid username or password'})
    return render(request, 'login.html')

@login_required
def student_dashboard(request):
    return render(request, 'student_dashboard.html')

@login_required
def admin_dashboard(request):
    return render(request, 'admin_dashboard.html')


def student_dashboard(request):
    if request.method == 'POST':
        if 'logout' in request.POST: 
            logout(request) 
            return redirect('user_login')
        book_title = request.POST.get('book_title', '')
        print("book_title:::::", book_title)
        if book_title:
            author, number_of_pages = fetch_book_details(book_title)
            print("number_of_pages:::::", number_of_pages)
            if number_of_pages:
                book, created = Book.objects.get_or_create(
                    title=book_title,
                    # number_of_pages = number_of_pages
                    defaults={'author': author, 'number_of_pages': number_of_pages}
                )
                return render(request, 'student_dashboard.html', {'book': book})
            else:
                return render(request, 'student_dashboard.html', {'error': f'Book details not found for title ==> ({book_title})'})
    return render(request, 'student_dashboard.html')

def fetch_book_details(title):
    encoded_title = quote(title)
    url = f"https://openlibrary.org/search.json?title={encoded_title}"
    response = requests.get(url)
    if response.status_code == 200:
        data = response.json()
        if 'docs' in data and data['docs']:
            book_info = data['docs'][0]
            author = book_info.get('author_name', 'Unknown Author')
            number_of_pages_median = book_info.get('number_of_pages_median', 0)
            return author, number_of_pages_median
    return 'Unknown Author', 0


@login_required
def rent_book(request):
    if request.method == 'POST':
        book_id = request.POST.get('book_id')
        student = request.user.email
        print("student::::;;", student)
        book = Book.objects.get(pk=book_id)
        
        if Rental.objects.filter(user__email=student, book=book).exists():
            return render(request, 'student_dashboard.html', {'error': 'You have already rented this book.'})
        
        rental_date = timezone.now()
        return_date = rental_date + timedelta(days=30)  # Calculate return date
        
        rental = Rental.objects.create(user=AddedUser.objects.get(email=student), book=book, rental_date=rental_date, return_date=return_date)
                
        return redirect('rented_books_details')
    
    return redirect('student_dashboard')

@login_required
def rented_books_details(request):
    rented_books = Rental.objects.filter(user__email=request.user.email)
    return render(request, 'rented_books.html', {'rented_books':rented_books})


@login_required
def admin_dashboard(request):
    if request.method == 'POST':
        if 'logout' in request.POST: 
            logout(request) 
            return redirect('user_login')
        if 'add_book' in request.POST:
            title = request.POST.get('title')
            author = request.POST.get('author')
            number_of_pages = request.POST.get('number_of_pages')
            book = Book.objects.create(title=title, author=author, number_of_pages=number_of_pages)
            return redirect('admin_dashboard')
        elif 'change_rental_date' in request.POST:
            rental_id = request.POST.get('rental_id')
            new_rental_date = request.POST.get('new_rental_date')
            rental = Rental.objects.get(pk=rental_id)
            rental.rental_date = new_rental_date
            rental.save()
            return redirect('admin_dashboard')

    rentals = Rental.objects.all()
    for rental in rentals:
        rental.fee = rental.calculate_rental_fee()
        rental.save()

    return render(request, 'admin_dashboard.html', {'rentals': rentals})
