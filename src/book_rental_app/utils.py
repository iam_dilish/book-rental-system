import requests

def fetch_book_details(title):
    url = f"https://openlibrary.org/search.json?title={title}"
    response = requests.get(url)
    if response.status_code == 200:
        data = response.json()
        if 'docs' in data and data['docs']:
            return data['docs'][0].get('number_of_pages', 0)
    return 0
