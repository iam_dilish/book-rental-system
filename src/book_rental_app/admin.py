from django.contrib import admin
from .models import AddedUser, Book, Rental

admin.site.register(Book)
admin.site.register(Rental)

class AddedUserAdmin(admin.ModelAdmin):
    list_display = ('name','role')
    search_fields = ('name', 'email')
    
admin.site.register(AddedUser, AddedUserAdmin)