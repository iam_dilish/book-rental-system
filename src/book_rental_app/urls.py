from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name='home'),
    path('login/', views.user_login, name='user_login'),
    path('student/', views.student_dashboard, name='student_dashboard'),
    path('staff_admin/', views.admin_dashboard, name='admin_dashboard'),
    path('rent_book/', views.rent_book, name='rent_book'),
    path('rented_books_details/', views.rented_books_details, name='rented_books_details'),

]
