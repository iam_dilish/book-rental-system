# Generated by Django 5.0.4 on 2024-04-25 18:59

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("book_rental_app", "0002_user_role"),
    ]

    operations = [
        migrations.RenameModel(
            old_name="User",
            new_name="AddedUser",
        ),
    ]
