# populate_users.py

import os
import django
import sys
from django.utils import timezone

# Add the parent directory of the Django project to the Python path
parent_directory = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
sys.path.append(parent_directory)

# Set the Django settings module
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'book_rental_project.settings')

# Initialize Django
django.setup()

# Import the User model
# from book_rental_app.models import User
from django.contrib.auth.models import User

# Function to create users
def create_users():
    user_data = [
        {"username": "John Doe", "email": "john@example.com", "is_staff": "False", "is_active": "True", "password": "password"},
        {"username": "Jane Smith", "email": "jane@example.com", "is_staff": "False", "is_active": "True", "password": "password"},
        {"username": "Alice Johnson", "email": "alice@example.com", "is_staff": "False", "is_active": "True", "password": "password"},
        # Add more user data as needed
    ]

    for data in user_data:
        User.objects.get_or_create(username=data['username'], email=data['email'], is_staff=data['is_staff'], password=data['password'], date_joined=timezone.now())

# Run the function if the script is executed directly
if __name__ == '__main__':
    create_users()
