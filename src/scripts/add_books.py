
import os
import django
import sys

parent_directory = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
sys.path.append(parent_directory)

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'book_rental_project.settings')

django.setup()

from book_rental_app.models import User, Book


def create_books():
    book_data = [
        {"title": "Book Title 1", "author": "Author 1", "number_of_pages": 200},
        {"title": "Book Title 2", "author": "Author 2", "number_of_pages": 300},
        {"title": "Book Title 3", "author": "Author 3", "number_of_pages": 150},
    ]

    for data in book_data:
        Book.objects.get_or_create(title=data['title'], author=data['author'], number_of_pages=data['number_of_pages'])

if __name__ == '__main__':
    create_books()
