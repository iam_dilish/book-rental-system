
import os
import django
import sys

parent_directory = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
sys.path.append(parent_directory)

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'book_rental_project.settings')

django.setup()

from book_rental_app.models import User

def create_users():
    user_data = [
        {"name": "John Doe", "email": "john@example.com", "role": "Admin"},
        {"name": "Jane Smith", "email": "jane@example.com", "role": "Student"},
        {"name": "Alice Johnson", "email": "alice@example.com", "role": "Student"},
    ]

    for data in user_data:
        User.objects.get_or_create(name=data['name'], email=data['email'], role=data['role'])


if __name__ == '__main__':
    create_users()
