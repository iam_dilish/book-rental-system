# Book-Rental-System

## Introduction
This is a simple **Book Rental System** built with Django, allowing users to rent books and administrators to manage rentals and books.

## Prerequisites
- Python 3.x installed on your system
- pip package manager

## Installation
1. Clone the repository:
git clone https://gitlab.com/iam_dilish/book-rental-system.git

2. Navigate to the project directory:
cd book-rental-system/
cd src/

3. Install dependencies: pip install -r requirements.txt


## Setup
1. Apply migrations to set up the database:
python manage.py migrate

2. Create a superuser to access the admin panel:
python manage.py createsuperuser

**Follow the prompts to create a superuser account.**

## Usage
1. Run the development server:
python manage.py runserver

2. Access the application in your web browser at [http://localhost:8000/](http://localhost:8000/)

3. Login with your superuser credentials to access the admin panel.

4. For students, login with your registered credentials to access the student dashboard.

## Features
- **User Authentication**: Users can login and access their respective dashboards.
- **Student Dashboard**: Students can rent books and view rented books.
- **Admin Dashboard**: Administrators can manage books, view rentals, and change rental dates.
- **Book Search**: The system fetches book details from Open Library API based on the entered title.

## API Used
- [Open Library API](https://openlibrary.org/dev/docs/api/search)

## File Structure
- `book-rental-system/`
- `src/`
- `README.md`: Instructions and overview of the project.
- `requirements.txt`: List of Python dependencies.
- `manage.py`: Django management script.
- `book_rental_project/`
 - `settings.py`: Django settings file.
 - `urls.py`: URL configuration.
 - `book_rental_app/`
 - `views.py`: Views for handling HTTP requests.
 - `urls.py`: URL configuration.
 - `models.py`: Database models.
 - `templates/`: HTML templates.
 - `scripts/`: To add data to the existing tables.
- `db.sqlite3`: SQLite database file.


